import Vue from 'vue';
import Router from 'vue-router';
import store from '../store/index';

Vue.use(Router);

import LoginComponent from '@/components/LoginComponent'
import HomeComponent from  '@/components/HomeComponent';
import FormComponent from "../components/FormComponent";

const router = new Router({
    mode: 'history',
    routes:[
        {
            path : '/',
            component : LoginComponent
        },
        {
            path : '/Form/:id?',
            name: 'Form',
            component : FormComponent,
            meta :{

              requiresAuth: true ,
            }
        },
        {
            path:'/Home',
            name:'Home',
            component: HomeComponent,
            meta :{ requiresAuth: true}
        }
    ]
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isAuthenticated) {
            next()
            return
        }
        next('/')
    } else {

        next()
    }
})

export  default  router;
