import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
   state:{
       registros:[],
       user : localStorage.getItem('user') || '',
   },
   mutations:{
       setRegistros(state,registro){
           state.registros.push(registro);
       },
       deleteRegistro(state,id) {
           state.registros.splice(id, 1);
       },
   },
    actions:{
        logOut(){
            localStorage.removeItem('user')
            window.location.href="Home";
        },
    },
    getters:{
       getRegistros(state){
           return state.registros;
       },
        getRegistrosByKey: (state) => (id) => {
            return state.registros[id];
        },
        isAuthenticated: state => !!state.user,
        getUser: state => (state.user != '') ?  JSON.parse(state.user) : '',
    }
});
